<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockpostcodenl}prestashop>blockpostcodenl_a273933936228569c7d477c75b765742'] = 'Ongeldig postcode formaat, gebruik \'1234AB\'';
$_MODULE['<{blockpostcodenl}prestashop>blockpostcodenl_659c6c3120abe719b5717d354fab7b66'] = 'Onbekende postcode + huisnummer combinatie.';
$_MODULE['<{blockpostcodenl}prestashop>blockpostcodenl_655c56f0a7578923a5d1739de78b4e29'] = 'Validatie fout, gebruik a.u.b. de handmatige invoer';
$_MODULE['<{blockpostcodenl}prestashop>blockpostcodenl_8cdeabc66933b9a1889e3302205d13cd'] = 'Adres validatie';
$_MODULE['<{blockpostcodenl}prestashop>blockpostcodenl_47406f2de1102fdc42968c3b1404cd21'] = 'Voer je postcode en huisnummer in om je adres automatisch in te vullen.';
$_MODULE['<{blockpostcodenl}prestashop>blockpostcodenl_8bcdc441379cbf584638b0589a3f9adb'] = 'Postcode';
$_MODULE['<{blockpostcodenl}prestashop>blockpostcodenl_b6da88038814604ebbccdbb558c2e7d4'] = 'Huisnummer';
$_MODULE['<{blockpostcodenl}prestashop>blockpostcodenl_127cd45034d39b3b466aa83fbfc7f249'] = 'Huisnummer toevoeging';
$_MODULE['<{blockpostcodenl}prestashop>blockpostcodenl_dd9ad9189b84e4ab08bc94af34f6e140'] = 'Voer adres handmatig in';
